package com.multitiers.equ03.Multitiers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultitiersApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultitiersApplication.class, args);
	}

}
